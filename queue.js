let collection = [];

// Write the queue functions below.


function print() {
    return collection
} 

function enqueue(element){
    collection.push(element)
    return collection
}

function dequeue(element){
    collection.shift()
    return collection
}

function peek(element){
	return collection[0]
}

function length(){
	collection.length()
	return collection
}

function isEmpty(){
	return collection.length === 0
}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	peek,
	length,
	isEmpty	
};